package com.example.renat.calculator;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

    TextView textResult; //вывод результата
    EditText textNumber; //ввод числа
    TextView textOperation; //вывод знака операции
    Double aDouble = null;
    String strOperation = "=";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textResult = findViewById(R.id.result);
        textNumber = findViewById(R.id.numbers);
        textOperation = findViewById(R.id.operation);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString("OPERATION", strOperation);
        if(aDouble!=null)
            outState.putDouble("OPERAND", aDouble);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        strOperation = savedInstanceState.getString("OPERATION");
        aDouble = savedInstanceState.getDouble("OPERAND");
        textResult.setText(aDouble.toString());
        textOperation.setText(strOperation);
    }

    public void onNumberClick(View view){

        Button button = (Button)view;
        textNumber.append(button.getText());

        if(strOperation.equals("=") && aDouble!=null){
            aDouble = null;
        }
    }

    public void onOperationClick(View view){

        Button button = (Button)view;
        String op = button.getText().toString();
        String number = textNumber.getText().toString();

        if(number.length()>0){
            number = number.replace(',', '.');
            try{
                performOperation(Double.valueOf(number), op);
            }catch (NumberFormatException ex){
                textNumber.setText("");
            }
        }
        strOperation = op;
        textOperation.setText(strOperation);
    }

    private void performOperation(Double number, String operation){

        if(aDouble==null){
            aDouble = number;
        }
        else{
            if(strOperation.equals("=")){
                strOperation = operation;
            }
            switch(strOperation){
                case "=":
                    aDouble=number;
                    break;
                case "/":
                    if(number==0){
                        aDouble=0.0;
                    }
                    else{
                        aDouble/=number;
                    }
                    break;
                case "*":
                    aDouble*=number;
                    break;
                case "+":
                    aDouble+=number;
                    break;
                case "-":
                    aDouble-=number;
                    break;
            }
        }
        textResult.setText(aDouble.toString().replace('.', ','));
        textNumber.setText("");
    }

    public void onBackPressed() {
        openQuitDialog();
    }

    private void openQuitDialog() {
        AlertDialog.Builder quitDialog = new AlertDialog.Builder(MainActivity.this);
        quitDialog.setTitle("Калькулятор больше не нужен?");

        quitDialog.setPositiveButton("Не нужен", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });

        quitDialog.setNegativeButton("Продолжим считать", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(getApplicationContext(), "Может лучше отдохнуть?", Toast.LENGTH_LONG).show();
            }
        });

        quitDialog.show();
    }
}
